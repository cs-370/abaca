# Building Abaca

Build the docker images for this project
```
    docker-compose build
```

Deploy the project locally
```
    docker-compose up -d
```

Note that the DB must be seeded using internal/api/storage/postgres/api.go  


### Basic Structure

The API subproject contains both the API and the HUB.


The Client subproject contains the HTML/CSS/JS for the client.


Deploy holds configuration files for these two projects and for nginx.